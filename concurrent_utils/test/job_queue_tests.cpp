#include "concurrent_utils/job_queue.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

class test_job1
{
public:
  test_job1(std::atomic_int *cnt) : cnt(cnt) { }
  test_job1() : cnt(nullptr) { }

  void run()
  {
    cnt->operator++();
  }

private:
  std::atomic_int *cnt = nullptr;
};


BOOST_AUTO_TEST_CASE( insert_wait_test )
{
  job_queue<test_job1, fifo_buffer, false> queue(2, 2);
  std::atomic_int run_cnt = ATOMIC_VAR_INIT(0);
  BOOST_CHECK(!queue.is_running());

  queue.start(false);
  for (unsigned i = 0; i < 10; ++i) {
    queue.add_job(test_job1(&run_cnt));
  }
  while (!queue.empty()) { }
  queue.stop();
  BOOST_CHECK_MESSAGE(run_cnt == 10, "Adding jobs to a full and running job-queue should result in the calling thread "
                                     "to wait for a slot in the job-queue, because the job-queue is specified "
                                     "to block when full.");

  queue.add_job(test_job1());
  BOOST_CHECK_MESSAGE(queue.empty(), "Adding jobs while not running the job-queue should omit the job");
}

BOOST_AUTO_TEST_CASE( insert_omit_test_1 )
{
  job_queue<test_job1, fifo_buffer, true> queue(2, 2);
  std::atomic_int run_cnt = ATOMIC_VAR_INIT(0);
  BOOST_CHECK(!queue.is_running());

  queue.start(false);
  for (unsigned i = 0; i < 10; ++i) {
    queue.add_job(test_job1(&run_cnt));
  }
  while (!queue.empty()) { }
  queue.stop();
  BOOST_CHECK_MESSAGE(run_cnt <= 10, "Jobs can get omitted when the job-queue is full and configured to "
                                     "omit jobs in this case.");

  queue.add_job(test_job1());
  BOOST_CHECK_MESSAGE(queue.empty(), "Adding jobs while not running the job-queue should omit the job");
}

class test_job2
{
public:
  test_job2(std::atomic_bool *finish_adding, std::atomic_int *cnt) : finish_adding(finish_adding), cnt(cnt) { }
  test_job2() { }

  void run()
  {
    cnt->operator++();
    while (!*finish_adding) { }
  }

private:
  std::atomic_bool *finish_adding = nullptr;
  std::atomic_int *cnt = nullptr;
};

BOOST_AUTO_TEST_CASE( insert_omit_test_2 )
{
  job_queue<test_job2, fifo_buffer, true> queue(2, 2);
  std::atomic_bool finished_adding = ATOMIC_VAR_INIT(false);
  std::atomic_int run_cnt = ATOMIC_VAR_INIT(0);

  BOOST_CHECK(!queue.is_running());

  queue.start(false);

  queue.add_job(test_job2(&finished_adding, &run_cnt));
  while (!queue.empty()) { }
  queue.add_job(test_job2(&finished_adding, &run_cnt));
  while (!queue.empty()) { }

  // queue is empty now but both consumer threads are blocked

  for (unsigned i = 0; i < 5; ++i) { // all but 2 should get omitted
    queue.add_job(test_job2(&finished_adding, &run_cnt));
  }

  std::thread stop([&] {
    while (!queue.empty()) { } // execute all remaining jobs in the queue before stopping
    queue.stop();
  });

  finished_adding = true;
  stop.join();

  BOOST_CHECK_MESSAGE(run_cnt == 4, "The number of executed jobs should be exactly 4 because "
                                    "two jobs are executed and two are fitting into the "
                                    "queue. The rest gets emitted because the queue is full "
                                    "and the first two jobs are waiting until finished_adding is "
                                    "set to true.");
}


BOOST_AUTO_TEST_CASE( blocking_mode_test )
{
  job_queue<test_job2, fifo_buffer> queue(2, 2);

  std::thread stopper([&] {
    while (!queue.is_running()) { }
    queue.stop();
  });

  queue.start(true); // blocking mode
  stopper.join();
  BOOST_CHECK(!queue.is_running());
}
