#include "concurrent_utils/job_queue_buffer.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE( general_test )
{
  fifo_buffer<int> fifo(4);
  BOOST_CHECK(fifo.empty());

  fifo.insert(7);
  BOOST_CHECK(!fifo.empty());

  fifo.insert(8);
  fifo.insert(9);

  BOOST_CHECK(fifo.remove_next() == 7);

  fifo.insert(42);
  fifo.insert(17);

  BOOST_CHECK(fifo.full());

  BOOST_CHECK(fifo.remove_next() == 8);
  BOOST_CHECK(fifo.remove_next() == 9);
  BOOST_CHECK(fifo.remove_next() == 42);
  BOOST_CHECK(fifo.remove_next() == 17);

  BOOST_CHECK(fifo.empty());

  fifo.insert(42);
  BOOST_CHECK(!fifo.empty());
  BOOST_CHECK(!fifo.full());
  BOOST_CHECK(fifo.remove_next() == 42);

  BOOST_CHECK(fifo.empty());
}
