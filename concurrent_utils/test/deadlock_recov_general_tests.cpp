#include "concurrent_utils/deadlock_recovery.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>


class my_resource : public transactional
{
protected:
  void on_begin_transaction() override
  {
    locked = true;
  }
  void on_end_transaction() override
  {
    locked = false;
  }
  void rollback() override
  {
    rollbacked = true;
  }

public:
  unsigned int id = 0;
  bool locked = false;
  bool rollbacked = false;
};


BOOST_AUTO_TEST_CASE( single_transaction_test )
{
  my_resource resources[3];
  auto get_resource = [&resources](unsigned int id) -> my_resource& {
    return resources[id];
  };
  for (unsigned int i = 0; i < sizeof (resources) / sizeof (*resources); ++i) { // prepare
    get_resource(i).id = i;
  }
  deadlock_recovery<my_resource, unsigned int> dr(get_resource);

  unsigned int selected = 2;

  EXEC_TRANSACTION(dr) {
    BOOST_CHECK(!get_resource(selected).locked);

    my_resource &res = dr.aquire_write(selected);
    BOOST_CHECK(res.id == selected);
    BOOST_CHECK(res.locked);
    BOOST_CHECK(!res.rollbacked);

    BOOST_CHECK(get_resource(selected).locked);
  };
  BOOST_CHECK(!get_resource(selected).locked);
}

BOOST_AUTO_TEST_CASE( deadlock_transaction_test )
{
  my_resource resources[5];
  auto get_resource = [&resources](unsigned int id) -> my_resource& {
    return resources[id];
  };
  for (unsigned int i = 0; i < sizeof (resources) / sizeof (*resources); ++i) { // prepare
    get_resource(i).id = i;
  }
  deadlock_recovery<my_resource, unsigned int> dr(get_resource);

  std::atomic_bool t0_ready = ATOMIC_VAR_INIT(false);
  std::atomic_bool t1_ready = ATOMIC_VAR_INIT(false);

  std::atomic_int transaction_exec_count = ATOMIC_VAR_INIT(0);
  std::atomic_int res_action_count = ATOMIC_VAR_INIT(0);

  std::thread t0([&] {
    EXEC_TRANSACTION(dr) {
      transaction_exec_count++;

      my_resource &res = dr.aquire_write(0);
      BOOST_CHECK(res.id == 0);
      t0_ready = true;

      while (!t1_ready) { } // wait until t1 locked resource 1

      dr.aquire_write(1);
      res_action_count++;
    };
  });

  std::thread t1([&] {
    EXEC_TRANSACTION(dr) {
      transaction_exec_count++;

      my_resource &res = dr.aquire_write(1);
      BOOST_CHECK(res.id == 1);
      t1_ready = true;

      while (!t0_ready) { } // wait until t0 locked resource 0

      dr.aquire_write(0);
      res_action_count++;
    };
  });

  t0.join();
  t1.join();

  BOOST_CHECK(!get_resource(0).locked);
  BOOST_CHECK(!get_resource(1).locked);
  BOOST_CHECK_MESSAGE(transaction_exec_count == 3, "Exactly one of the two transaction should be executed "
                                                   "twice because of a deadlock");
  BOOST_CHECK_MESSAGE(res_action_count == 2, "Each resource action should be called exactly one time");
  BOOST_CHECK_MESSAGE(get_resource(0).rollbacked != get_resource(1).rollbacked,
                      "Exactly one of the two transactions should have a rollback because of a deadlock");
}

BOOST_AUTO_TEST_CASE( circular_deadlock_test )
{
  my_resource resources[5];
  auto get_resource = [&resources](unsigned int id) -> my_resource& {
    return resources[id];
  };
  for (unsigned int i = 0; i < sizeof (resources) / sizeof (*resources); ++i) { // prepare
    get_resource(i).id = i;
  }
  deadlock_recovery<my_resource, unsigned int> dr(get_resource);

  std::atomic_int transaction_exec_count = ATOMIC_VAR_INIT(0);

  std::atomic_bool t0_ready = ATOMIC_VAR_INIT(false);
  std::atomic_bool t1_ready = ATOMIC_VAR_INIT(false);
  std::atomic_bool t2_ready = ATOMIC_VAR_INIT(false);

  std::thread t0([&] {
    EXEC_TRANSACTION(dr) {
      transaction_exec_count++;

      my_resource &res = dr.aquire_write(0);
      BOOST_CHECK(res.id == 0);
      t0_ready = true;

      while (!t1_ready) { } // wait until t1 locked resource 1

      dr.aquire_write(1);
    };
  });

  std::thread t1([&] {
    EXEC_TRANSACTION(dr) {
      transaction_exec_count++;

      my_resource &res = dr.aquire_write(1);
      BOOST_CHECK(res.id == 1);
      t1_ready = true;

      while (!t2_ready) { } // wait until t1 locked resource 1

      dr.aquire_write(2);
    };
  });

  std::thread t2([&] {
    EXEC_TRANSACTION(dr) {
      transaction_exec_count++;

      my_resource &res = dr.aquire_write(2);
      BOOST_CHECK(res.id == 2);
      t2_ready = true;

      while (!t0_ready) { } // wait until t1 locked resource 1

      dr.aquire_write(0);
    };
  });

  t0.join();
  t1.join();
  t2.join();

  BOOST_CHECK(!get_resource(0).locked);
  BOOST_CHECK(!get_resource(1).locked);
  BOOST_CHECK(!get_resource(2).locked);

  BOOST_CHECK_MESSAGE(transaction_exec_count == 4, "Exactly one of the two transaction should be executed "
                                                   "twice because of a circular deadlock");

  std::vector<bool> rollbacked = {get_resource(0).rollbacked, get_resource(1).rollbacked, get_resource(2).rollbacked};
  int rollback_count = static_cast<int>(std::count(rollbacked.begin(), rollbacked.end(), true));
  BOOST_CHECK_MESSAGE(rollback_count == 1, "In a circular deadlock with three transactions "
                                           "exactly one transaction should be rollbacked. Because of resource "
                                           "forwarding the rollback-transaction should not be able to reaquire "
                                           "the critical resource before the transaction which waited on this "
                                           "critical resource.");
}

BOOST_AUTO_TEST_CASE( atomic_test )
{
  my_resource resources[5];
  auto get_resource = [&resources](unsigned int id) -> my_resource& {
    return resources[id];
  };
  for (unsigned int i = 0; i < sizeof (resources) / sizeof (*resources); ++i) { // prepare
    get_resource(i).id = i;
  }
  deadlock_recovery<my_resource, unsigned int> dr(get_resource);

  BOOST_CHECK(dr.aquire_write(3).id == 3);
  // not allowed to lock any extra resource while holding the resource 3
  // because this could result in a deadlock.
  // Atomic locks are allowed because of performance.
  dr.release(3);
}
