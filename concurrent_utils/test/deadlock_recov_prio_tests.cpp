#include "concurrent_utils/deadlock_recovery.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>


class test_resource : public transactional
{
public:

protected:
  void on_begin_transaction() override
  {
    access_count++;
    std::string msg = "resource should be accessable by only one transaction at a time: rid=" + std::to_string(id);
    BOOST_REQUIRE_MESSAGE(access_count == 1, msg);
  }
  void on_end_transaction() override
  {
    access_count--;
    std::string msg = "resource should be accessable by only one transaction at a time: rid=" + std::to_string(id);
    BOOST_REQUIRE_MESSAGE(access_count == 0, msg);
  }
  void rollback() override
  {
    rollback_count++;
  }

public:
  std::atomic_int access_count = ATOMIC_VAR_INIT(0);
  std::atomic_int rollback_count = ATOMIC_VAR_INIT(0);
  unsigned int id = 0;
};


/*!
 * This test starts three transactions in the threads t0, t1 and t2 and creates a circular deadlock between them.
 * Exactly one of them gets restarted which increases its priority for the next deadlock situation.
 * After this deadlock got resolved, a transaction in thread t3 gets started which waits for a resource
 * of the previously restarted transaction which is still running because it is waiting on t3 as well.
 * This second deadlock between the previously restarted transaction and the transaction in t3 should
 * result in t3 getting restarted because the other transaction has a higher priority because it got already
 * restarted once previously. This means 2 transactions are restarted once and the other two transactions
 * are restarted zero times.
 */
BOOST_AUTO_TEST_CASE( deadlock_recov_prio_test1 )
{
  test_resource resources[5];
  unsigned int resource_size = 5;

  auto get_resource = [&resources](unsigned int id) -> test_resource& {
    return resources[id];
  };
  for (unsigned int i = 0; i < resource_size; ++i) { // prepare
    get_resource(i).id = i;
  }
  deadlock_recovery<test_resource, unsigned int> dr(get_resource);

  std::atomic_int exec_cnt[4];
  unsigned int exec_cnt_size = 4;

  for (unsigned int i = 0; i < sizeof(exec_cnt) / sizeof(*exec_cnt); ++i) {
    exec_cnt[i] = 0;
  }
  std::atomic_int retry_cnt = ATOMIC_VAR_INIT(0);
  std::atomic_int retried_transaction = ATOMIC_VAR_INIT(-1);


  std::atomic_bool t0_ready = ATOMIC_VAR_INIT(false);
  std::atomic_bool t1_ready = ATOMIC_VAR_INIT(false);
  std::atomic_bool t2_ready = ATOMIC_VAR_INIT(false);
  std::atomic_bool t3_ready = ATOMIC_VAR_INIT(false);

  std::string retry_once_msg = "Onle one transaction should get retried due to resource forwarding "
                               "which prevents a race between the victim transaction and the "
                               "winner-transaction.";

  // will get executed inside exactly one of the three threads t0, t1 and t2 inside the transaction environment
  auto on_retry = [&](int t) {
    retried_transaction = t; // just one of the resources locked by the executing transaction
    retry_cnt++;

    BOOST_REQUIRE_MESSAGE(retry_cnt == 1, retry_once_msg);

    while (!t3_ready) { } // needed to create a second deadlock with thread t3 to test priority increase
    dr.aquire_write(3);
  };

  std::thread t0([&] {
    EXEC_TRANSACTION(dr) {
      exec_cnt[0]++;

      dr.aquire_write(0);
      while (!t1_ready) { }
      t0_ready = true;
      dr.aquire_write(1);

      if (exec_cnt[0] > 1) { // true if this transaction is retried more than once
        on_retry(0);
      }
    };
  });
  std::thread t1([&] {
    EXEC_TRANSACTION(dr) {
      exec_cnt[1]++;

      dr.aquire_write(1);
      t1_ready = true;
      while (!t2_ready) { }
      dr.aquire_write(2);

      if (exec_cnt[1] > 1) { // true if this transaction is retried more than once
        on_retry(1);
      }
    };
  });
  std::thread t2([&] {
    EXEC_TRANSACTION(dr) {
      exec_cnt[2]++;

      dr.aquire_write(2);
      t2_ready = true;
      while (!t0_ready) { }
      dr.aquire_write(0);

      if (exec_cnt[2] > 1) { // true if this transaction is retried more than once
        on_retry(2);
      }
    };
  });


  std::thread t3([&] {
    EXEC_TRANSACTION(dr) {
      exec_cnt[3]++;

      dr.aquire_write(4); // just to increase priority of this transaction

      while (retried_transaction == -1) { } // waits for one of the three transactions inside t0, t1 or t2 to retry/rollback

      dr.aquire_write(3);
      BOOST_REQUIRE(resources[3].access_count == 1);
      t3_ready = true;

      // the parameter which stores the retried transaction (either in t0, t1 or t2)
      // also identifies a resource which is currently locked by the retried transaction.
      // this should result in a deadlock and retry of this transaction
      dr.aquire_write(static_cast<unsigned int>(retried_transaction));

      BOOST_REQUIRE_MESSAGE(exec_cnt[3] == 2, "This transaction should get retried once and therefore get executed "
                                              "twice because it competes with another transaction "
                                              "which already got retried once before and which therefore has "
                                              "a higher priority than this transaction.");
    };
  });


  t0.join();
  t1.join();
  t2.join();
  t3.join();

  BOOST_REQUIRE_MESSAGE(retry_cnt == 1, retry_once_msg);
  for (unsigned int i = 0; i < resource_size; ++i) {
    BOOST_CHECK_MESSAGE(resources[i].access_count == 0, "On the end after all transaction executed "
                                                        "every resource should be free again.");
  }

  BOOST_TEST(exec_cnt[3] == 2);
  BOOST_TEST(exec_cnt[retried_transaction] == 2);
  for (unsigned int i = 0; i < exec_cnt_size; ++i) {
    if (i != 3 && i != static_cast<unsigned int>(retried_transaction)) {
      BOOST_TEST(exec_cnt[i] == 1);
    }
  }
}

BOOST_AUTO_TEST_CASE( deadlock_recov_prio_test2 )
{
  test_resource resources[3];

  auto get_resource = [&resources](unsigned int id) -> test_resource& {
    return resources[id];
  };
  deadlock_recovery<test_resource, unsigned int> dr(get_resource);

  std::atomic_bool t0_ready = ATOMIC_VAR_INIT(false);
  std::atomic_bool t1_ready = ATOMIC_VAR_INIT(false);

  std::atomic_int t0_exec_cnt = ATOMIC_VAR_INIT(0);
  std::atomic_int t1_exec_cnt = ATOMIC_VAR_INIT(0);

  std::thread t0([&] {
    EXEC_TRANSACTION(dr) {
      t0_exec_cnt++;

      dr.aquire_write(2);
      dr.aquire_write(0);

      t0_ready = true;
      while (!t1_ready) { }
      dr.aquire_write(1);
    };
  });

  std::thread t1([&] {
    EXEC_TRANSACTION(dr) {
      t1_exec_cnt++;

      dr.aquire_write(1);
      t1_ready = true;
      while (!t0_ready) { }
      dr.aquire_write(0);
    };
  });

  t0.join();
  t1.join();

  BOOST_CHECK(t0_exec_cnt == 1);
  BOOST_CHECK_MESSAGE(t1_exec_cnt == 2, "The retried transaction should always be in thread t1 "
                                        "because it aquires one resource less than the transaction "
                                        "in thread t0 (before the deadlock). This means that t1 has less"
                                        "resources to rollback and is therefore selected as victim.");
}
