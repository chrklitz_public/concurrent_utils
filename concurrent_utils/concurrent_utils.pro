TEMPLATE = app
CONFIG += c++14
CONFIG -= qt
CONFIG += console
CONFIG -= debug_and_release debug_and_release_target

DEFINES += CONCURRENT_UTILS_LIB
DEFINES += QT_DEPRECATED_WARNINGS

include(boost_include.pri)

LIBS += -lpthread

SOURCES +=

HEADERS += \
  concurrent_utils/deadlock_recovery.hpp \
  concurrent_utils/detail/transaction_manager.hpp \
  concurrent_utils/job_queue.hpp \
  concurrent_utils/job_queue_buffer.hpp

test {
  include(test/testsources.pri)
} else {
  SOURCES += concurrent_utils/main.cpp
}
