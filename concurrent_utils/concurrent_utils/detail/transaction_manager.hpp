#ifndef TRANSACTION_MANAGER_H
#define TRANSACTION_MANAGER_H

#include <thread>
#include <set>
#include <chrono>
#include <mutex>
#include <map>
#include <vector>


namespace detail {


using transaction_id = std::thread::id;

enum RESOURCE_LOCK_STATE
{
  AVAILABLE, OWNED_BY_ATOMIC, OWNED_BY_TRANSACTION
};

/*!
 * Provides management for transaction meta-data needed for the deadlock recovery.
 */
template<typename resource_id>
class transaction_manager
{
  struct transaction_data
  {
    std::set<resource_id> owned_resources;

    // true means a rollback for the transaction (needed for condition_variable)
    bool undo_mark = false;

    // true means that this resource gets directly transfered to another transction
    // (needed for condition_variable)
    bool forwarded_resource = false;

     // higher values mean higher time until timeout when a transaction waits for anotherone
    uint8_t timeout_level = 0;

    // gets increased on transaction rollbacks to prevent starvation
    size_t total_prio = 0;
  };

  static const uint8_t max_timeout_level = 10;
  std::chrono::milliseconds exp_timeout_base = std::chrono::milliseconds(2); // max = 100 + 2^10 (ms) =~ 1.1sec
  std::chrono::milliseconds min_timeout_duration = std::chrono::milliseconds(100);

public:
  class transaction
  {
  public:
    transaction(transaction_manager &t_manager, transaction_id id) : t_manager(t_manager), t_id(id) { }

    transaction_id get_id() const { return t_id; }

    bool is_valid() const {
      std::lock_guard<std::mutex> lck(t_manager.m);
      return t_manager.t_data.find(get_id()) != t_manager.t_data.end();
    }

    bool owns_resource(resource_id id) const
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      auto &owned = t_manager.t_data[get_id()].owned_resources;
      return owned.find(id) != owned.end();
    }
    std::set<resource_id> &owned_resources() const
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      return t_manager.t_data[get_id()].owned_resources;
    }

    void clear_for_retry() //
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      auto current_prio = t_manager.t_data[get_id()].total_prio;
      t_manager.t_data[get_id()] = {};
      t_manager.t_data[get_id()].total_prio = current_prio + 1;
    }
    bool marked_undo() const
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      return t_manager.t_data[get_id()].undo_mark;
    }

    void mark_undo()
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      t_manager.t_data[get_id()].undo_mark = true;
    }

    // does not set the state of the resource! -> Change implementation to match!
    template<typename resource_handle>
    void aquire_resource(resource_id id, resource_handle &res)
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      res.resource_lock_state = OWNED_BY_TRANSACTION;
      t_manager.t_data[get_id()].owned_resources.insert(id);
      reset_timeout_duration_impl();
    }

    std::chrono::milliseconds timeout_duration() {
      std::lock_guard<std::mutex> lck(t_manager.m);
      auto extra = t_manager.exp_timeout_base * static_cast<int>(1 << t_manager.t_data[get_id()].timeout_level);
      return t_manager.min_timeout_duration + extra;
    }
    void increase_timeout_duration()
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      if (t_manager.t_data[get_id()].timeout_level < max_timeout_level)
        ++t_manager.t_data[get_id()].timeout_level;
    }
    void reset_timeout_duration()
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      reset_timeout_duration_impl();
    }

    bool forwarded_resource()
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      return t_manager.t_data[get_id()].forwarded_resource;
    }
    void confirm_resource_forwarding()
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      t_manager.t_data[get_id()].forwarded_resource = false;
    }

    resource_id wait_marked_resource() // only makes sense if this transaction previosly was marked to wait on a resource
    {
      std::lock_guard<std::mutex> lck(t_manager.m);
      return t_manager.deadlock_waitinglist.find(get_id())->second;
    }

    void mark_transaction_wait_for_resource(resource_id rid) {
      std::lock_guard<std::mutex> lck(t_manager.m);
      t_manager.deadlock_waitinglist[get_id()] = rid;
    }
    void unmark_transaction_wait_for_resource() {
      std::lock_guard<std::mutex> lck(t_manager.m);
      auto it = t_manager.deadlock_waitinglist.find(get_id());
      if (it != t_manager.deadlock_waitinglist.end()) {
        t_manager.deadlock_waitinglist.erase(it);
      }
    }

    // This definition of a priority also allows "brute-force" transactions with a maximum total_prio pre-set.
    // Those transactions would displace all other transactions which do not have the same total_prio.
    //
    // NOT thread safe by its own. Defines the priority between two transactions
    bool operator<(const transaction &other) const
    {
      transaction_data &this_data = t_manager.t_data[get_id()];
      transaction_data &other_data = other.t_manager.t_data[other.get_id()];

      return this_data.total_prio < other_data.total_prio
          || this_data.owned_resources.size() < other_data.owned_resources.size();
    }


  private:
    inline void reset_timeout_duration_impl() { t_manager.t_data[get_id()].timeout_level = 0; }

  private:
    transaction_manager &t_manager;
    transaction_id t_id;
  };

  transaction get_transaction(transaction_id id) { return transaction(*this, id); }

  bool is_transaction(transaction_id id)
  {
    std::lock_guard<std::mutex> lck(m);
    return t_data.find(id) != t_data.end();
  }

  void add_transaction(transaction_id id)
  {
    std::lock_guard<std::mutex> lck(m);
    t_data[id] = {};
  }
  void remove_transaction(transaction_id id)
  {
    std::lock_guard<std::mutex> lck(m);
    t_data.erase(id);
  }

  void forward_resource(transaction_id tid1, transaction_id tid2, resource_id rid) {
    std::lock_guard<std::mutex> lck(m);
    t_data[tid1].owned_resources.erase(rid);
    t_data[tid2].owned_resources.insert(rid);
    t_data[tid2].forwarded_resource = true;
    // std::cout << "forwarding resource rid=" << rid << " from tid=" << tid1 << " to tid=" << tid2 << std::endl;
  }



  /*!
   * \brief find_deadlock finds a deadlock if existing based on marked dependencies between transactions and resources.
   * It starts at root and checks if the corresponding transaction is in a deadlock.
   *
   * \param root transaction id of the starting transaction
   * \return a vector x representing a ringbuffer where x[i] waits on x[i+1 % x.size()]. x can be empty which means
   * that no deadlock was found with root transaction in it. If x is not empty it has a minimum size of 2 and root id
   * is at index 0 -> x[0] == root will get evaluated to true.
   */
   std::vector<transaction_id> find_deadlock(transaction_id root) {
    std::map<transaction_id, transaction_id> deadlock_graph;

    bool contains_root = false;

    // Create deadlock graph which shows waiting dependencies between transactions
    for (auto waiting : deadlock_waitinglist) {
      for (auto other : deadlock_waitinglist) {
        if (waiting.first != other.first) {
          if (get_transaction(other.first).owns_resource(waiting.second)) {
            deadlock_graph[waiting.first] = other.first;
            if (waiting.first == root)
              contains_root = true;
            break;
          }
        }
      }
    }

    if (deadlock_graph.size() < 2 || !contains_root) // less then two edges
      return {};

    // graph with outdegree of max 1
    std::vector<transaction_id> dl_transactions = {root};
    dl_transactions.reserve(deadlock_graph.size()); // maximum size

    transaction_id current = deadlock_graph[root];
    while (current != root && deadlock_graph.find(current) != deadlock_graph.end()) {
      dl_transactions.push_back(current);
      current = deadlock_graph[current];
    }

    return dl_transactions;
  }

private:
  std::mutex m;
  std::map<transaction_id, transaction_data> t_data;
  std::map<transaction_id, resource_id> deadlock_waitinglist; // move into transactions_manager
};

} // end namespace detail


#endif // TRANSACTION_MANAGER_H
