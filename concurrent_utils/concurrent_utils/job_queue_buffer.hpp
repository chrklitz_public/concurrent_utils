#ifndef JOB_QUEUE_BUFFER_HPP
#define JOB_QUEUE_BUFFER_HPP

#include <type_traits>
#include <vector>
#include <queue>

namespace detail {
template <typename T, typename U, typename = void>
struct has_less_than_operator : std::false_type { };

template <typename T, typename U>
struct has_less_than_operator<T, U, decltype((std::declval<T>() < std::declval<U>()), void())> : std::true_type { };
} // end namespace


template<typename T>
class fifo_buffer
{
  static_assert (std::is_default_constructible<T>::value, "template parameter type T needs to be default constructable");

public:
  fifo_buffer(size_t capacity)
  {
    _ring_buffer.resize(capacity);
  }

  void insert(const T &element)
  {
    size_t index = (_begin + _size++) % _ring_buffer.size();
    _ring_buffer[index] = element;
  }

  T remove_next()
  {
    _size--;
    size_t old_begin = _begin;
    _begin = (_begin + 1) % _ring_buffer.size();
    return _ring_buffer[old_begin];
  }

  bool full() const
  {
    return _size == _ring_buffer.size();
  }
  bool empty() const
  {
    return _size == 0;
  }

private:
  std::vector<T> _ring_buffer;
  size_t _size = 0;
  size_t _begin = 0;
};

template<typename T>
class priority_queue_buffer
{
  static_assert (detail::has_less_than_operator<T, T>::value, "This strategy is for elements with priorities");

public:
  priority_queue_buffer(size_t capacity)
    : _capacity(capacity)
  {

  }

  void insert(const T &element)
  {
    _pqueue.push(element);
  }
  T remove_next()
  {
    T top = _pqueue.top();
    _pqueue.pop();
    return top;
  }
  bool full() const
  {
    return _pqueue.size() == _capacity;
  }
  bool empty() const
  {
    return _pqueue.empty();
  }

private:
  std::priority_queue<T> _pqueue;
  size_t _capacity;
};


#endif // JOB_QUEUE_BUFFER_HPP
