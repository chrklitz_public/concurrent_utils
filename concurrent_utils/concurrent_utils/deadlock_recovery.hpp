#ifndef DEADLOCK_RECOVERY_H
#define DEADLOCK_RECOVERY_H


#include <functional>
#include <condition_variable>
#include <algorithm>

#include "detail/transaction_manager.hpp"


#define TRANSACTION [&]() -> void
#define EXEC_TRANSACTION(dlr) dlr << [&]() -> void
#define RESOURCE_ACTION(res_t) [&](res_t) -> void


template<typename resource, typename resource_id>
class deadlock_recovery;


/*!
 * \brief The transactional class is an interface for resources which should be managable
 * by the deadlock_recovery class.
 */
class transactional
{
private:
  template<typename resource, typename resource_id>
  friend class deadlock_recovery;
  template<typename resource_id>
  friend class detail::transaction_manager;

protected:
  /*!
   * \brief on_begin_transaction marks the begin of a transaction execution on this resource.
   * When calling rollback(), changes made to this object after this function-call must be
   * reversed.
   */
  virtual void on_begin_transaction() = 0;

  /*!
   * \brief on_end_transaction marks the end of a transaction execution on this resource.
   * Changes made to this object can be made permanent.
   */
  virtual void on_end_transaction() = 0;

  /*!
   * \brief rollback undos all changes made to this object since the last on_begin_transaction() call.
   * Calling this function has also the effect of the function on_end_transaction().
   */
  virtual void rollback() = 0;

private:
  detail::RESOURCE_LOCK_STATE resource_lock_state = detail::AVAILABLE;

public:
  virtual ~transactional() = default;
};


/*!
 * \brief The deadlock_recovery class implements deadlock recovery on multiple data/resource objects of a single type.
 *
 * To be applicable with this class the resource type needs to inherit from the transactional class and implement
 * its functions (protected). A transaction which is a callable object compatible to std::function
 * can be executed multiple times in case of a deadlock which means that
 * it shouldn't have side-effects besides the accessed resources.
 *
 * \param resource is the type of the managed resource. Its class needs to inherit from class transactional.
 * \param resource_id is the id used by this class to access resources via a lambda provided in the constructor.
 */
template<typename resource, typename resource_id>
class deadlock_recovery
{
  static_assert (std::is_base_of<transactional, resource>::value, "template resource type needs to inherit from class transactional");

  using transaction = std::function<void(void)>;

  // from the past where resources and handles being different objects (not supported any more)
  using resource_handle = transactional;

private:
  // thrown on deadlock, to directly leave the execution context of the transaction lambda.
  struct deadlock_undo_exception {};
  enum THREE_CASE : char { TRUE, FALSE, OTHER };

public:
  /*!
   * \brief constructs an object of type deadlock_recovery by taking a resource getter function as input
   * \param get_resource is the callable object wich returns a resource reference for its corresponding it.
   * With the same id the function should always return the same resource reference or at least an accessor
   * to the same underlying data.
   */
  deadlock_recovery(std::function<resource&(resource_id id)> get_resource);

  /*!
   * \brief runs a function on a single resource.
   * \param func is the function which is called together with the resource.
   * \param rid is the resource id which defines the resource passed to func
   */
  resource &aquire_write(resource_id rid);

  void aquire_read(resource_id rid); // TODO: implement this

  // releases the resource. Releasing a resource inside a transaction has no effect.
  void release(resource_id rid);

  /*!
   * \brief exec runs a function (transaction) on potentially multiple resources.
   * In case of a deadlock t may be executed again which means that it shouldn't have side-effects
   * besides the accessed resources. A transaction can execute atomic resource functions with
   * exec(resource_action, resource_id) inside it. Those resource accesses will be handled differently and
   * are potentially more time intensive than resource accesses without an outer transaction because
   * the transaction environment with data-consistency between resource-accesses comes with the cost of potential
   * deadlocks and therefore rollbacks and retries of entire transactions.
   *
   * \param t transaction function
   */
  void exec_transaction(transaction t);

  /*!
   * \brief operator << executes a transaction and has the same functionality like exec(transaction).
   * \param t transaction function
   * \return reference to this object (return *this)
   */
  deadlock_recovery &operator<<(transaction t)
  {
    exec_transaction(t);
    return *this;
  }

private:


  void aquire_write_without_transaction(resource_handle &handle);
  void release_without_transaction(resource_handle &handle);

  // shortcut/convenience functions:
  // in the past there was a differentiation between handle and resource which is the reason
  // for the existence of the get_resource_handle function.
  inline resource_handle &get_resource_handle(resource_id rid) { return get_resource(rid); }
  inline detail::transaction_id current_transaction_id() const { return std::this_thread::get_id(); }

  // Marks all resources owned by the current transaction context (current thread) as AVAILABLE and lets
  // the resources know that the transaction has finished, so that they can free potentially made backups.
  void make_transaction_resources_available();

  // Causes a rollback on all owned resources of the current transaction context (current thread).
  void rollback_transaction_resources();


  // enter with glob_lck locked. returns with glob_lck unlocked
  // return TRUE: Means resource aquirement successful
  // return FALSE: resource aquirement failed and deadlock discovered for the calling transaction
  // return OTHER: Not all cases are covered by this function in which case this function returns this
  THREE_CASE aquire_resource_from_atomic(std::unique_lock<std::mutex> &glob_lck, resource_id rid);

  // enter with glob_lck locked. returns with glob_lck unlocked
  // return true: Means resource aquirement successful
  // return false: resource aquirement failed and deadlock discovered for the calling transaction
  bool aquire_resource_from_transaction(std::unique_lock<std::mutex> &glob_lck, resource_id rid);


private:
  detail::transaction_manager<resource_id> transactions;
  std::function<resource&(resource_id id)> get_resource;

  std::mutex global_mutex;
  std::condition_variable global_cv;
};

// ========================== IMPLEMENTATION ===================================

template<typename resource, typename resource_id>
deadlock_recovery<resource, resource_id>::deadlock_recovery(std::function<resource &(resource_id)> get_resource)
  : get_resource(get_resource)
{ }

template<typename resource, typename resource_id>
resource &deadlock_recovery<resource, resource_id>::aquire_write(resource_id rid)
{
  using namespace detail;

  resource &res = get_resource(rid);
  resource_handle &handle = get_resource_handle(rid);
  auto current_transaction = transactions.get_transaction(current_transaction_id());

  if (current_transaction.is_valid()) { // if this is actually part of a transaction

    // shortcut without global lock to boost performance
    // Also required to prevent multiple locks of the same resource from the same transaction!
    if (current_transaction.owns_resource(rid)) {
      return res;
    }

    std::unique_lock<std::mutex> glob_lck(global_mutex);

    // Case 1: Assumes that there currently is no potential deadlock-situation which means that
    // the requested resource is not owned by another transaction.
    // Because the resource could still be locked by a transaction-less atomic resource action this
    // part still has to deal with potential deadlock-situations. It does this by just forwarding
    // in case the resource is locked by another transaction. This case is then handled by case 2.
    THREE_CASE result = aquire_resource_from_atomic(glob_lck, rid);

    if (result == TRUE) {
      return res;
    } else if (result == FALSE) {
       throw deadlock_undo_exception();
    } else {
      // Case 2 (OTHER): The requested resource is currently owned by another transaction which indicates a potential
      // deadlock. This case handles it by waiting for the resource to get released but with timeouts
      // to prevent infinite waiting. This case handles all possible states but for performance reasons the
      // first case is executed before to filter out most of the resource requests.
      glob_lck.lock();
      if (!aquire_resource_from_transaction(glob_lck, rid))
        throw deadlock_undo_exception();

      return res;
    }

  } else { // just an action on a single resource -> does not need deadlock and consistency management
    aquire_write_without_transaction(handle);
    return res;
  }
}

template<typename resource, typename resource_id>
void deadlock_recovery<resource, resource_id>::release(resource_id rid)
{
  if (!transactions.is_transaction(current_transaction_id())) {
    release_without_transaction(get_resource_handle(rid));
  }
}

template<typename resource, typename resource_id>
void deadlock_recovery<resource, resource_id>::exec_transaction(deadlock_recovery::transaction func)
{
  // std::cout << "Start transaction tid=" << current_transaction_id() << std::endl;

  auto ta = transactions.get_transaction(current_transaction_id());

  if (ta.is_valid())
    throw std::invalid_argument("Nested transactions are not supported");

  transactions.add_transaction(ta.get_id()); // setup in transaction manager

  bool retry = true;
  while (retry) {
    try {
      retry = false;
      func();
      make_transaction_resources_available();
    } catch (deadlock_undo_exception) { // potentially add some logging by adding a message to the exception
      // std::cout << "retry transaction: " << ta.get_id() << std::endl;
      rollback_transaction_resources();
      make_transaction_resources_available();
      // transactions.get_transaction(ta.get_id()).clear_for_retry();
      // unmarks them as redo and cleans transaction data
      ta.clear_for_retry();
      retry = true;
    }
  }

  transactions.remove_transaction(ta.get_id());
  // std::cout << "Finish transaction tid=" << current_transaction_id() << std::endl;
}

template<typename resource, typename resource_id>
void deadlock_recovery<resource, resource_id>::aquire_write_without_transaction
(deadlock_recovery::resource_handle &handle)
{
  using namespace detail;

  // aquire resource
  std::unique_lock<std::mutex> glob_lck(global_mutex);

  global_cv.wait(glob_lck, [&] { return handle.resource_lock_state == AVAILABLE; });
  handle.resource_lock_state = OWNED_BY_ATOMIC;
  glob_lck.unlock();
}

template<typename resource, typename resource_id>
void deadlock_recovery<resource, resource_id>::release_without_transaction
(deadlock_recovery::resource_handle &handle)
{
  using namespace detail;
  // release resource
  {
    std::lock_guard<std::mutex> glob_lck(global_mutex);
    handle.resource_lock_state = AVAILABLE;
  }
  global_cv.notify_all();
}

template<typename resource, typename resource_id>
void deadlock_recovery<resource, resource_id>::make_transaction_resources_available()
{
  std::lock_guard<std::mutex> glob_lck(global_mutex);
  for (resource_id rid : transactions.get_transaction(current_transaction_id()).owned_resources()) {
    get_resource_handle(rid).on_end_transaction();
    get_resource_handle(rid).resource_lock_state = detail::AVAILABLE;
  }
  global_cv.notify_all();
}

template<typename resource, typename resource_id>
void deadlock_recovery<resource, resource_id>::rollback_transaction_resources()
{
  std::lock_guard<std::mutex> glob_lck(global_mutex);
  for (resource_id rid : transactions.get_transaction(current_transaction_id()).owned_resources()) {
    get_resource_handle(rid).rollback();
  }
}

template<typename resource, typename resource_id>
typename deadlock_recovery<resource, resource_id>::THREE_CASE
deadlock_recovery<resource, resource_id>::aquire_resource_from_atomic
(std::unique_lock<std::mutex> &glob_lck, resource_id rid)
{
  using namespace detail;

  auto &handle = get_resource_handle(rid);
  auto current_transaction = transactions.get_transaction(current_transaction_id());

  if (handle.resource_lock_state != OWNED_BY_TRANSACTION) { // this extra test is just for performance

    auto cond = [&] {
      return handle.resource_lock_state == AVAILABLE
          || current_transaction.marked_undo()
          || handle.resource_lock_state == OWNED_BY_TRANSACTION; // to prevent infinite waiting in case of a deadlock
    };

    global_cv.wait(glob_lck, cond);

    if (handle.resource_lock_state == AVAILABLE) {
      current_transaction.aquire_resource(rid, handle);
      glob_lck.unlock();
      handle.on_begin_transaction();

      return TRUE;
    } else if (current_transaction.marked_undo()) {
      transactions.get_transaction(current_transaction_id()).unmark_transaction_wait_for_resource();
      glob_lck.unlock();
      return FALSE;
    } else { // case OWNED_BY_TRANSACTION
      // This case should rarely happen and means that the resource access will get retried
      // with timeouts by another function which can deal with resources which are locked by another transaction.
    }
  }
  glob_lck.unlock();
  return OTHER;
}

template<typename resource, typename resource_id>
bool deadlock_recovery<resource, resource_id>::aquire_resource_from_transaction
(std::unique_lock<std::mutex> &glob_lck, resource_id rid)
{
  using namespace detail;

  auto current_transaction = transactions.get_transaction(current_transaction_id());
  auto &handle = get_resource_handle(rid);

  auto release_cond = [&] {
    return handle.resource_lock_state == AVAILABLE
        || current_transaction.marked_undo()
        || current_transaction.forwarded_resource();
  };

  // Repeatadly tries to lock the resource with a timeout
  while (true) {
    if (global_cv.wait_for(glob_lck, current_transaction.timeout_duration(), release_cond)) {
      // ===== ON RESOURCE AQUIRING SUCCESS OR UNDO =====

      // transactions.get_transaction(current_transaction_id()).unmark_transaction_wait_for_resource();
      current_transaction.unmark_transaction_wait_for_resource(); // removes dependency from the deadlock graph

      if (current_transaction.marked_undo()) {
        glob_lck.unlock();
        return false;

      } else if (handle.resource_lock_state == AVAILABLE) {
        current_transaction.aquire_resource(rid, handle);
        current_transaction.reset_timeout_duration();
        glob_lck.unlock();

        handle.on_begin_transaction();
        return true;

      } else { // forwarded resource -> already aquired
        current_transaction.confirm_resource_forwarding(); // unmarks it from being forwarded
        current_transaction.reset_timeout_duration();
        glob_lck.unlock();

        return true;
      }

    } else { // failed to access resource in time -> search for deadlock:
      // =================== ON TIMEOUT ==================

      // transactions.get_transaction(current_transaction_id()).mark_transaction_wait_for_resource(rid);
      current_transaction.mark_transaction_wait_for_resource(rid); // add dependency to deadlock graph
      std::vector<transaction_id> deadlock_transactions = transactions.find_deadlock(current_transaction_id());

      if (!deadlock_transactions.empty()) { // ON DEADLOCK
        // find victim out of deadlock_transactions set:
        // -> search transaction with highest prio and make it the winner. The transaction after that is the victim
        // auto dl_it = std::max_element(deadlock_transactions.begin(), deadlock_transactions.end());
        auto dl_it = deadlock_transactions.begin();

        for (auto it = deadlock_transactions.begin() + 1; it != deadlock_transactions.end(); ++it) {
          if (transactions.get_transaction(*dl_it) < transactions.get_transaction(*it))
            dl_it = it;
        }

        transaction_id winner_tid = *dl_it;
        ++dl_it;
        transaction_id victim_tid;
        if (dl_it == deadlock_transactions.end())
          victim_tid = deadlock_transactions[0];
        else
          victim_tid = *dl_it;

        // winner waits for a resource from the victim which is calculated here
        resource_id forwarded_resource = transactions.get_transaction(winner_tid).wait_marked_resource();

        // std::cout << "tid=" << current_transaction_id() << " recognizes deadlock victim tid=" << victim_tid << ", winner tid=" << winner_tid << std::endl;

        // resource forwarding to prevent the victim transaction to just re-aquire
        // the same resource after rollback and restart
        transactions.forward_resource(victim_tid, winner_tid, forwarded_resource);

        // required because the victim transaction won't do the rollback because it got forwarded
        get_resource_handle(forwarded_resource).rollback();

        if (victim_tid == current_transaction.get_id()) { // undo calling transaction
          // transactions.get_transaction(current_transaction_id()).unmark_transaction_wait_for_resource();
          current_transaction.unmark_transaction_wait_for_resource();
          glob_lck.unlock();
          return false;

        } else { // mark other transaction as undo and continue here
          transactions.get_transaction(victim_tid).mark_undo();
          global_cv.notify_all();
          continue;
        }
      } else { // NO DEADLOCK FOUND
        // std::cout << "increase timeout time tid=" << current_transaction_id() << " try to lock " << rid << std::endl;
        current_transaction.increase_timeout_duration();
        continue;
      }
    } // end else
  } // while loop
}





#endif // DEADLOCK_RECOVERY_H
