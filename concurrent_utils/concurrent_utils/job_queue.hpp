#ifndef JOB_QUEUE_HPP
#define JOB_QUEUE_HPP

#include <thread>
#include <functional>
#include <vector>
#include <condition_variable>
#include <mutex>
#include <memory>
#include <algorithm>
#include <atomic>

#include <iostream>

#include "job_queue_buffer.hpp"


template<typename job,
         template<typename> class strategy_buffer = fifo_buffer,
         bool omit_if_full = false>
class job_queue
{
  using buffer = strategy_buffer<job>;

public:
  job_queue(size_t buffer_size, unsigned int thread_count = std::thread::hardware_concurrency())
    : _buffer(buffer_size)
  {
    _thread_pool.resize(thread_count, nullptr);
  }


  void start(bool blocking = false)
  {
    {
      std::unique_lock<std::mutex> lck(_buffer_mutex);

      if (_running)
        return;

      _running = true;

      // start threads:
      for (unsigned int i = 0; i < _thread_pool.size(); ++i) {
        _thread_pool[i] = std::make_shared<std::thread>([this] {
          thread_loop();
        });
      }
    }

    if (blocking) {
      std::unique_lock<std::mutex> lck(_buffer_mutex);
      std::atomic_bool &running = _running;
      _blocking_mode_cv.wait(lck, [&running] { return !running; });
    }
  }
  void stop(bool blocking = true)
  {
    {
      std::lock_guard<std::mutex> lck(_buffer_mutex);
      if (!_running)
        return;

      _running = false;
      _blocking_mode_cv.notify_all();
    }

    _remove_cv.notify_all(); // wake up all threads which wait on a resource
    _insert_cv.notify_all();

    if (blocking)
      join_all();
  }

  bool add_job(const job &j)
  {
    std::unique_lock<std::mutex> lck(_buffer_mutex);

    if (!_running) // omits job
      return false;

    if (omit_if_full) {
      if (_buffer.full())
        return false;
    } else {
      auto &b = _buffer;
      auto &running = _running;
      _insert_cv.wait(lck, [&b, &running] { return !b.full() || !running; });
    }

    if (!_running)
      return false;

    _buffer.insert(j);
    _remove_cv.notify_one();
    return true;
  }

  bool is_running() const { return _running; }
  bool empty() const
  {
    std::lock_guard<std::mutex> lck(_buffer_mutex);
    return _buffer.empty();
  }

  ~job_queue()
  {
    this->stop(); // blocking
  }

private:
  void thread_loop() // executed by the threads in the thread-pool
  {
    while (_running) {
      std::unique_lock<std::mutex> lck(_buffer_mutex);
      _remove_cv.wait(lck, [&] { return !_buffer.empty() || !_running; });
      if (_running) { // buffer not empty
        job next = _buffer.remove_next();
        _insert_cv.notify_one();
        lck.unlock();
        next.run();
      }
    }
  }

  void join_all()
  {
    for (auto &t : _thread_pool)
      if (t != nullptr) t->join();
  }

private:
  buffer _buffer;
  mutable std::mutex _buffer_mutex;
  std::mutex _start_stop_mutex;
  std::condition_variable _insert_cv;
  std::condition_variable _remove_cv;
  std::condition_variable _blocking_mode_cv; // used for the blocking mode

  std::vector<std::shared_ptr<std::thread>> _thread_pool;
  std::atomic_bool _running = ATOMIC_VAR_INIT(false);
};















#endif // JOB_QUEUE_HPP
