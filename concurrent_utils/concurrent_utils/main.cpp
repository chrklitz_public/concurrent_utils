#include <iostream>

#include "deadlock_recovery.hpp"

using resource_id = unsigned int;

class my_resource : public transactional
{
protected:
  void on_begin_transaction() override;
  void on_end_transaction() override;
  void rollback() override;

public:
  resource_id id;
  int data = 0;
};

void foo(my_resource &) { }

int main() {
  std::cout << "Hello world" << std::endl;

  deadlock_recovery<my_resource, resource_id> dr(nullptr);

  EXEC_TRANSACTION(dr) {

  };

}
