# concurrent_utils

Contains hopefully helpful utility for concurrent C++ programming like a deadlock-recovery implementation.

This project currently does not have any useful documentation. 
Use `test/deadlock_recov_*.cpp` test files as a **demo** on how to use the **deadlock_recovery** class.
